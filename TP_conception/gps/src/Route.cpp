#include "Route.hpp"

bool Route::operator==(const Route & r) const {
    if(((villeA_==r.villeA_ and villeB_==r.villeB_)
            or (villeB_==r.villeA_ and villeA_==r.villeB_))
            and distance_==r.distance_) return true;
    return false;
}

bool Route::operator!=(const Route & r) const {
    return not (*this==r);
}
