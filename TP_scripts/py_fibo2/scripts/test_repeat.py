#!/usr/bin/env python3

import repeat
import fibo

def print_fibo(n):
    print(fibo.fiboIterative(n))

repeat.repeatN(print_fibo,10)
