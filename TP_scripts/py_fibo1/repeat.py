#!/usr/bin/env python3

def repeatN(f,n):
    for i in range(1,n+1):
        f(i)
