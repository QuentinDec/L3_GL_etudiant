#include <cmath>
#include <pybind11/pybind11.h>

float sinus(float a, float b,float x) {
	float ret =  sin((float)2*M_PI*a*x+b);
	printf("%f\n",ret);
	return ret;
}


PYBIND11_PLUGIN(sinus) {
	pybind11::module m("sinus");
	m.def("sinus",&sinus);
	return m.ptr();
}
