#ifndef CHRONO_HPP_
#define CHRONO_HPP_
/// \file Chrono.hpp
#include <chrono>

/** 
 * \class Chrono 
 * \brief Chronomètre pour mesurer des durées.
 * Exemple d'utilisation :
 * \code
 *      Chrono chrono;
 *      chrono.start();
 *      ...
 *      chrono.stop();
 *      std::cout << "temps écoulé : " << chrono.elapsed() << std::endl;
 * \endcode
 */
class Chrono {

    private:
        /// \privatesection
        std::chrono::time_point<std::chrono::system_clock> _t0;
        std::chrono::time_point<std::chrono::system_clock> _t1;
        bool _isRunning;

    public:
        /// \publicsection
        /// \brief Constructeur.
        Chrono();

        /// \brief Remet la mesure à zéro. \n
        /// Ne change pas l'état démarré/arrêté du chronomètre.
        void reset();

        /// \brief Remet à zéro et démarre une nouvelle mesure.
        void start();

        /// \brief Arrête la mesure.
        void stop();

        /// \brief Retourne le temps écoulé. \n
        /// Ne change pas l'état démarré/arrêté du chronomètre.
        double elapsed();
};

#endif

