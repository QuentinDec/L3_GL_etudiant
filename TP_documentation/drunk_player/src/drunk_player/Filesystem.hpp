#ifndef FILESYSTEM_HPP_
#define FILESYSTEM_HPP_
/// \file Filesystem.hpp
#include <string>
#include <vector>

/// \brief Retourne la liste des fichiers contenus dans un dossier.\n
/// Les fichiers sont donnés avec leur chemin relatif.
std::vector<std::string> getFilesInFolder(const std::string & pathname);

#endif

