.. drunk_player documentation master file, created by
   sphinx-quickstart on Wed Mar 27 11:16:18 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Drunk_player_cli
========================================

Résumé
------------------------

Drunk_player est un système de lecture de vidéos qui a trop bu. Il lit les vidéos contenues dans un dossier par morceaux, aléatoirement et parfois en transformant l’image.

Drunk_player utilise la bibliothèque de traitement d’image OpenCV et est composé :

- d’une bibliothèque (drunk_player) contenant le code de base
- d’un programme graphique (drunk_player_gui) qui affiche le résultat à l’écran
- d’un programme console (drunk_player_cli) qui sort le résultat dans un fichier



Sommaire
------------------------
- :ref:`Installation`
	+ Dépendances
	+ Récupérer les sources
- Drunk_player_cli
	+ Description
	+ Utilisation

.. code-block:: cpp

	int main() {
		int a=42;
		return 0;
	}

.. figure:: ../../drunk_player_gui.png
	:width: 40%

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

