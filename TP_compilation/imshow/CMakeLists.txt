cmake_minimum_required( VERSION 3.0 )
project( MON_PROJET )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )

find_package( PkgConfig REQUIRED )

pkg_check_modules( PKG_OPENCV REQUIRED opencv )
include_directories( ${PKG_OPENCV_INCLUDE_DIRS} )
add_executable( imshow.out imshow.cpp )
target_link_libraries( imshow.out ${PKG_OPENCV_LIBRARIES} )

#Si on modifie un fichier il suffit de  recompiler avec le Makefile
#Si on ajoute un fichier on doit modifier le CMakeLists.txt pour qu'il soit prit en compte
